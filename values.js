function values (obj) {
  if (!typeof obj === 'object' || obj === null) {
    return null
  }

  const output= []
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      output.push(obj[key])
    }
  }
  return output;
}

module.exports = values
