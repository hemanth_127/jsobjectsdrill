function mapObject (obj, cb) {
  if(!(typeof obj)==="object" || obj===null){
    return null
  }

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      console.log(cb(obj[key]))
    }
  }
}

module.exports = mapObject
