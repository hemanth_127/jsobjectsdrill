function defaults (obj, defaultProps) {
  if (
    !(typeof obj === 'object') ||
    obj === null ||
    !typeof defaultProps === 'object'
  ) {
    return null
  }

  for (key in obj) {
    if (obj[key] === undefined) {
      obj[key] = defaultProps
    }
  }
  return obj
}

module.exports = defaults
