function pairs (obj) {
  if (!(typeof obj === 'object') || obj === null) {
    return null
  }

  let output = []
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      output.push([key, obj[key]])
    }
  }

  console.log(output)
}

module.exports = pairs
